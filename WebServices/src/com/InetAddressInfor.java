
package com;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class InetAddressInfor {
	
	public static void main(String[] args) throws SocketException {
		System.out.println("get All NetWordInterfaces\n");
		getAllNetWordInterfaces();
		System.out.println("\nsearchForMac\n");
		System.out.println(searchForMac());
	}
	
	public static void getNetWorkInterfaceById() {
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			
			System.out.println("Current IP address : " + ip.getHostAddress());
			
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			
			if (network == null) {
				System.out.println("Null NetworkInterface....");
				return;
			}
			
			byte[] mac = network.getHardwareAddress();
			
			System.out.print("Current MAC address : ");
			
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			System.out.println(sb.toString());
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	public static void getAllNetWordInterfaces() {
		try {
			
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (networkInterfaces.hasMoreElements()) {
				NetworkInterface network = networkInterfaces.nextElement();
				System.out.println("network : " + network);
				byte[] mac = network.getHardwareAddress();
				if (mac == null) {
					System.out.println("null mac");
				} else {
					System.out.print("MAC address : ");
					
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
					}
					System.out.println(sb.toString());
					break;
				}
			}
		} catch (SocketException e) {
			
			e.printStackTrace();
			
		}
	}
	
	public static String searchForMac() {
		try {
			String firstInterface = null;
			Map<String, String> addressByNetwork = new HashMap<>();
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			
			while (networkInterfaces.hasMoreElements()) {
				NetworkInterface network = networkInterfaces.nextElement();
				
				byte[] bmac = network.getHardwareAddress();
				if (bmac != null) {
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < bmac.length; i++) {
						sb.append(String.format("%02X%s", bmac[i], (i < bmac.length - 1) ? "-" : ""));
					}
					
					if (sb.toString().isEmpty() == false) {
						addressByNetwork.put(network.getName(), sb.toString());
						System.out.println("Address = " + sb.toString() + " @ [" + network.getName() + "] " +
							network.getDisplayName());
					}
					
					if (sb.toString().isEmpty() == false && firstInterface == null) {
						firstInterface = network.getName();
					}
				}
			}
			
			if (firstInterface != null) { return addressByNetwork.get(firstInterface); }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
